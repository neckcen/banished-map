Map Generator
=============

**[Download](https://github.com/neckcen/banished-map/releases/latest)** |
[Changelog](https://github.com/neckcen/banished-map/releases)

Map generator is a mod for [Banished](http://www.shiningrocksoftware.com/game/).
It gives you more options when generating a map for a new game.

Map Sizes
---------
Map generator adds two new map sizes: tiny and huge. Tiny maps offer half the
surface of a small map whilst huge maps give you twice the space of a large map.

Please note that huge maps can take a few minutes to be generated even on a
powerful computer.

Map Types
---------

[Sample of each type](http://i.imgur.com/8C0n2f2.jpg)

* **Mountains** Terrain looks sharper with taller peaks than in vanilla Banished,
otherwise unchanged. Backward compatible with existing seeds.
* **Valleys** Unchanged from vanilla game. Backward compatible with existing seeds.
* **Plains** Flatter than valleys with only a few hills here and there.
* **Swamps** Water rich maps with limited space available.

License
-------
Banished © 2014 Shining Rock Software. All Rights Reserved.

Map Generator mod © 2014 Sylvain Didelot. Released under the MIT Licence.

