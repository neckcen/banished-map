@echo off
set VERSION=2

echo What do you want to do?
echo   1. Build and test
echo   2. Build and package
echo   3. Build only

choice /n /c 123
set TODO=%errorlevel%

if EXIST bin (
    rmdir bin /s /q
)
..\bin\x64\Tools-x64.exe /build NeckcenMapResources.rsc /pathres ../banished-map/source /pathdat ../banished-map/bin
if %TODO% == 1 (
    ..\bin\x64\Application-x64-profile.exe /ref NeckcenMapResources.rsc /pathres ../banished-map/source /pathdat ../banished-map/bin
)
if %TODO% == 2 (
    ..\bin\x64\Tools-x64.exe /mod Package.rsc:NeckcenMap /pathres ../banished-map/source /pathdat ../banished-map/bin
    move ..\bin\winData\NeckcenMap.pkm NeckcenMap%VERSION%.pkm
)
rmdir bin /s /q
pause
