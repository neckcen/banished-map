StringTable NeckcenMapSize
{
    Entry _strings
    [
        {
            String _name = "TerrainSize0";
            String _text = "Tiny";
        }
        {
            String _name = "TerrainSize1";
            String _text = "Small";
        }
        {
            String _name = "TerrainSize2";
            String _text = "Medium";
        }
        {
            String _name = "TerrainSize3";
            String _text = "Large";
        }
        {
            String _name = "TerrainSize4";
            String _text = "Huge";
        }
    ]
}
StringTable NeckcenMap
{
    Entry _strings
    [
        {
            String _name = "Plains";
            String _text = "Plains";
        }
        {
            String _name = "Swamps";
            String _text = "Swamps";
        }
    ]
}
