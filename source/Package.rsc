PackageFile NeckcenMap
{
	String _name = "Map Generator";
	String _author = "Neckcen <neck+banished@eiky.net>";
	String _description = "More map options when starting a new game.";
    String _icon = "Icon.png";
    String _preview = "Preview.jpg";
	int _userVersion = 1;

	// all files in resource directory
	String _includeList
	[
		"*"
	]

	// exclude package files, mod files, file used for building packages
	String _excludeList
	[
		"Package_*.crs"
		"*.pkg"
		"*.pkm"
        "NeckcenMapResources_*.crs"
	]
}
