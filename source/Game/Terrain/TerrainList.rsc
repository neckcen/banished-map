// at least one of the map need to be in Game/Terrain/ or you get an exception
// after the map is created.
ExternalList resource
{
	External _resources
	[
        "Game/Terrain/Mountains.rsc",
        "Game/Terrain/Valleys.rsc",
        "Game/Terrain/Plains.rsc",
        "Game/Terrain/Swamps.rsc",
	]
}
